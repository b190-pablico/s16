console.log("Hello World")
/*
	javascript can also command our browsers to perform different arithmetic operations just like how mathematics work
*/
// ARITHMETIC OPERATORS SECTION

// creation of variables used in mathematics operations
let x = 1397;
let y = 7831;

/*
	Basic Operators
		+ addition operator
		- subtraction operator
		* multiplication operator
		/ division operator
		% modulo operator
*/

let sum = x + y;
console.log(sum);

let difference = x - y;
console.log(difference);

let product = x * y;
console.log(product);

let quotient = x / y;
console.log(quotient);

let remainder = y % x;
console.log(remainder);

// Assignment Operator
// = assignment operator and it is used to assign a value to a variable; the value on the right side of the operator is assigned to the left variable
let assignmentNumber = 8;

// addition assignment operator
// assignmentNumber = assignmentNumber + 2;
// shofthand for addition assignment(+=)
assignmentNumber += 2;
console.log(assignmentNumber);

// subtraction assignment operator
assignmentNumber -= 2;
console.log(assignmentNumber);

// multiplication assignment operator
assignmentNumber *= 2;
console.log(assignmentNumber);

// division assignment operator
assignmentNumber /= 2;
console.log(assignmentNumber);

// Multiple Operators and Parenthesis


/*
	when multiple operators are present in a single statement, it follows the PEMDAS(Parenthesis, Exponent, Multiplication, Division, Addition, Subtraction)
*/
/*
	MDAS
	The code below is computed base on the following:
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6
*/

/*
	PEMDAS
	The code below is computed base on the following:
		1. 2 - 3 = -1
		2. -1 * 4 = -4
		3. -4 / 5 = -0.8
		4. 1 + -0.8 = .2
*/
let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2 - 3) * 4 / 5;
console.log(pemdas);


pemdas = (1 + (2 - 3)) * (4 / 5);
console.log(pemdas);

// Increment and Decrement Section
// assigning a value to a variable to be used in increment and decrement section
let z = 1;
/*
	increment ++ - addutubg 1 to the value of the variable whether vefore or after the assigning of value3
		pre-increment - adding 1 to the value before it is assign to the variable
		post-increment = adding 1 to the value after it is assigned to the variable

*/
let increment = ++z
// the value of z is added by a value of 1 before returning the value and storing it inside the variable
console.log("Result of pre-increment: " + increment);
// the value of z was also increased by 1 even though we didn't explicitly specify any calue reassignment
console.log("Result of pre-increment: " + z);

increment = z++;
// the value of z is at 2 before it was incremented
console.log("Result of post-increment: "+ increment);
// the value of z was increased again reassigning the value to 3
console.log("Result of post-increment: "+ z);

/*
	decrement -- subtracting 1 to the value whether before or after assigning to the variable
*/
let decrement =--z;
console.log("Result of pre-drecrement: " + decrement);
console.log("Result of pre-drecrement: " + z);

decrement = --z;
console.log("Result of post-drecrement: " + decrement);
console.log("Result of post-drecrement: " + z);

// Type Coercion
/*
	is the automatic or implicit conversion of values from one data type to another
	this happens when operations are performed on different data types that would normally not be possible and yield irregular result
	values are automatically assigned/converted from one data type to another in order to resolbe operations
*/

let numbA = '10';
let numbB = 12;
let coercion = numbA + numbB;
console.log(coercion);

/*
try to have type coercion for the following

-number data + number data
-boolean + number
-boolean + string
*/
// num + num
let numbC = 16;
let numbD = 14;
// non coercion happens when the resulting data type is not really different from both of the original data types
let nonCoercion = numbC + numbD;
console.log(nonCoercion);

let varA = "String plus " + true;
console.log(varA);
console.log(typeof varA);

// Comparison Operators

// equality operator
/*
	checks whether the operands are equal/have the same content
*/
console.log(1==1);
console.log(1==2);

// equality operator
console.log(1 == "1");
console.log(1 == true);
console.log('juan' == 'juan');
let juan = 'juan';
console.log('juan' == juan);

// inequality operator
console.log(1 != 1);
console.log(1 != 2);
console.log(1 != '1');
console.log(0 != false);
console.log("juan" != 'juan');
console.log('juan' != juan);

// Strict equality
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === "1");
console.log(1 === true);
console.log('juan' === 'juan');
console.log('juan' === juan);

// inequality operator
console.log(1 !== 1);
console.log(1 !== 2);
console.log(1 !== '1');
console.log(0 !== false);
console.log("juan" !== 'juan');
console.log('juan' !== juan);

let a = 50;
let b = 65;

let greaterThan = a > b;

let lessThan = a < b;

let greaterThanOrEqualTo = a >= b;

let lessThanOrEqualTo = a <= b;

console.log(greaterThan);
console.log(lessThan);
console.log(greaterThanOrEqualTo);
console.log(lessThanOrEqualTo);

// true - product of a forced coercion to change the string into a number data type
let numStr = "30";
console.log(a > numStr);

// false - since a string is not numberic, the string was converted into a number and resulted into NaN 65 is not greater than NaN
	// NaN - Not a Numberl is the result of unsuccessful conversion of string into number data type of an alphanumeric string.
let str = "twenty";
console.log(b >= str);

// Logical Operators
/*
	checing whether the values of the two or more variables are true/false
*/
let isLegalAge = true;
let isRegistered = true;

// And Operator
// returns true if all values are true
/*
	1 			2 			end result
	true 		true 		true
	true 		false 		false
	false 		true 		false
	false 		false  		false
*/
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Reuls of And Operator: "+ allRequirementsMet);

// Or Operator (||)
// returns true if one of values is true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Or Operator: " + someRequirementsMet);

let someRequirementsNotMet = !isRegistered;
console.log("Result of Not Operator: " + someRequirementsNotMet);

